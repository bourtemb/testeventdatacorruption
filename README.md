# TestEventDataCorruption

## Description
This project was created to reproduce the problem identified in [cppTango#684](https://gitlab.com/tango-controls/cppTango/-/issues/684).  

## How to compile?

Open TestEventDataCorruption with Pogo and generate the Makefile or CMakeLists.txt for your installation.

### Using CMake

```
mkdir build
cd build
cmake ..
make -j
```

### Using Makefile

```
make -j
```

## Steps to reproduce [cppTango#684](https://gitlab.com/tango-controls/cppTango/-/issues/684)

1. Create a TestEventDataCorruption device server instance in your Tango Database. To reproduce the problem, it is important that this device server instances exports several devices.  
You can load TestEventDataCorruption.res file into jive (File menu --> Load property file) to create easily a TestEventDataCorruption/test instance exporting the following devices: test/ev-data-corruption/1, test/ev-data-corruption/2, test/ev-data-corruption/3 and test/ev-data-corruption/4.
1. Ensure you have TANGO_HOST environment variable correctly defined for your TANGO installation.
1. Start TestEventDataCorruption test instance (`TestEventDataCorruption test`)
1. Start an atkpanel on test/ev-data-corruption/1
1. Select MyLongArray tab to see the plot of the values of MyLongArray attribute
1. Start an atkpanel on test/ev-data-corruption/2
1. Select MyLongArray tab to see the plot of the values of MyLongArray attribute
1. Execute the command StartThread on test/ev-data-corruption/1 with value 111 as parameter. This will start a thread for this device which will push some change events for test/ev-data-corruption/1/MyLongArray attribute. The value of MyLongArray pushed by this thread will always be 10000 values of 111.
1. Execute the command StartThread on test/ev-data-corruption/2 with value 222 as parameter. This will start a thread for this device which will push some change events for test/ev-data-corruption/2/MyLongArray attribute. The value of MyLongArray pushed by this thread will always be 10000 values of 222.

At this stage, in my setup environment, I can see some glitches in the atkpanel clients MyLongArray plots where some values of test/ev-data-corruption/1/MyLongArray are containing some 222 and some values of test/ev-data-corruption/2/MyLongArray are containing some 111.
Feel free to Start additional threads in the other devices or even same devices to increase the chances to witness the problem reported in [cppTango#684](https://gitlab.com/tango-controls/cppTango/-/issues/684).




