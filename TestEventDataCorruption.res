#
# Resource backup , created Thu Sep 22 12:17:17 CEST 2022
#

#---------------------------------------------------------
# SERVER TestEventDataCorruption/test, TestEventDataCorruption device declaration
#---------------------------------------------------------

TestEventDataCorruption/test/DEVICE/TestEventDataCorruption: "test/ev-data-corruption/1",\ 
                                                             "test/ev-data-corruption/2",\ 
                                                             "test/ev-data-corruption/3",\ 
                                                             "test/ev-data-corruption/4"


# --- test/ev-data-corruption/1 attribute properties


# --- test/ev-data-corruption/2 attribute properties


# --- test/ev-data-corruption/3 attribute properties


# --- test/ev-data-corruption/4 attribute properties


#---------------------------------------------------------
# CLASS TestEventDataCorruption properties
#---------------------------------------------------------

CLASS/TestEventDataCorruption->Description: ""
CLASS/TestEventDataCorruption->InheritedFrom: TANGO_BASE_CLASS
CLASS/TestEventDataCorruption->ProjectTitle: "Class to test event data corruption issue reported in cppTango#684"

# CLASS TestEventDataCorruption attribute properties


